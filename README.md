# Manifiesto contra las plataformas privativas de correo en la universidad.

En los últimos meses, muchas universidades han pasado su correo
electrónico corporativo. Por ejemplo
en
[las universidades gallegas](https://twitter.com/gpul_/status/1153979399198466048) o
[en la universidad de Sevilla](https://twitter.com/skotperez/status/1149068916930621446). Esto
es un gran error, por las razones siguientes.

1. El correo es un sistema crítico hasta el punto que la dirección de
   correo suele servir como identificador personal del usuario. La
   externalización de un servicio crítico y esencial tal como este sería
   equivalente a que las clases de una universidad se impartieran por
   parte de una academia o una empresa privada.

1. Pasar de servidores propios a servidores ajenos puede [incumplir la
   normativa europea](https://www.genbeta.com/seguridad/prohiben-usar-escuelas-alemanas-office-365-nubes-apple-google-problemas-privacidad),
   incluso aunque los servidores en la nube estén alojados dentro de
   la unión europea.

1. En caso de que por reducción de costes se recurran a terceros, se hará de forma totalmente transparente, a través de un concurso público donde
    se garantice la imparcialidad para seleccionar los posibles candidatos, la empresa deberá llevar a rajatabla todos los puntos de la ley de protección de datos y 
    además, se prohibirá el uso y/o cesión de cualquier tipo de dato para fines distintos a la gestión del servicio.
